import datetime

from dateutil.relativedelta import relativedelta
from dateutil import rrule
from django.db import models
from django.utils.translation import gettext_lazy as _

from medux.core.models import BaseModel, Device, Person, Company


# TODO: make sure this is saved correctly and compliant to RFC 2445
# use...
# * https://github.com/dakrauth/django-swingtime
# * https://github.com/gregplaysguitar/django-eventtools
#

REPEAT_CHOICES = (
    ("RRULE:FREQ=HOURLY", _("Hourly")),
    ("RRULE:FREQ=DAILY", _("Daily")),
    ("RRULE:FREQ=WEEKLY", _("Weekly")),
    ("RRULE:FREQ=MONTHLY", _("Monthly")),
    ("RRULE:FREQ=MONTHLY;INTERVAL=2", _("Bimonthly")),
    ("RRULE:FREQ=MONTHLY;INTERVAL=3", _("Quarterly")),
    ("RRULE:FREQ=MONTHLY;INTERVAL=6", _("Semianually")),
    ("RRULE:FREQ=YEARLY", _("Yearly")),
    ("RRULE:FREQ=YEARLY;INTERVAL=2", _("Bianually")),
)


class DeviceAction(models.Model):
    """base model for inspections, briefings etc. on devices."""

    class Meta:
        abstract = True

    datetime = models.DateTimeField()

    executor = models.ForeignKey(
        Person,
        help_text=_("Person who did the action"),
        on_delete=models.PROTECT,
        related_name="+",
        null=True,
        blank=True,
    )


class DeviceInspection(DeviceAction):
    """A generic inspection on any device, with a timestamp, a person and a result."""

    result = models.TextField()


class DeviceBriefing(DeviceAction):
    instructed_person = models.ForeignKey(
        Person, on_delete=models.CASCADE, related_name="+"
    )


class Maintenance(DeviceAction):
    type = models.TextField(help_text=_("Type of maintenance"))


class DeviceIncident(DeviceInspection):
    """Security relevant device incidents (dysfunctions etc.)"""

    type = models.TextField(help_text=_("Type of incident"))
    consequences = models.TextField(blank=True)


class DeviceReport(DeviceAction):
    """A statutory report of an incident to an authority.

    These reports differ fom country to country, and can cover
        * dysfunctions with severe damage to a patient's health (or death)
        * priorly unknown side effects of treatment with device
        * severe quality defects
        * etc.
    """

    report = models.TextField()


class DeviceFile(BaseModel):
    """A file, defining the life span of a device."""

    device = models.OneToOneField(Device, on_delete=models.CASCADE)

    receiving_inspection = models.ForeignKey(
        DeviceInspection, on_delete=models.PROTECT, related_name="+"
    )

    briefing = models.ForeignKey(
        DeviceBriefing, on_delete=models.PROTECT, related_name="+"
    )

    security_inspection_interval = models.DurationField(choices=REPEAT_CHOICES)
    security_inspection_service_contract = models.ForeignKey(
        Company, on_delete=models.SET_NULL, related_name="+", null=True
    )
    security_inspections = models.ManyToManyField(DeviceInspection, related_name="+")

    measurement_inspection_interval = models.DurationField(choices=REPEAT_CHOICES)
    measurement_inspection_service_contract = models.ForeignKey(
        Company, on_delete=models.SET_NULL, related_name="+", null=True
    )
    measurement_inspections = models.ManyToManyField(DeviceInspection, related_name="+")

    maintenances = models.ManyToManyField(
        Maintenance, related_name="+", help_text=_("Planned maintenances")
    )
    maintenance_service_contract = models.ForeignKey(
        Company, on_delete=models.SET_NULL, related_name="+", null=True
    )

    incidents = models.ManyToManyField(DeviceIncident, related_name="+")

    out_of_service_date = models.DateField(blank=True, null=True)
