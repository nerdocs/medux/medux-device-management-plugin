from django.urls import path
from . import views

# namespaced URLs
app_name = "device_management"

# URLs namespaced  under device_management/
urlpatterns = [
    # path("", views.IndexView.as_view(), name="index"),
]


# global URLs
root_urlpatterns = [
    # path("api/foo", views.APIView.as_view(), name="api"),
]
